#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>

class TcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit TcpSocket(QObject *parent = 0);

    void Connect();

signals:

public slots:
private:
    QTcpSocket  *socket;
};

#endif // TCPSOCKET_H
