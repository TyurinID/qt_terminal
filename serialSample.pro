#-------------------------------------------------
#
# Project created by QtCreator 2016-05-25T11:56:37
#
#-------------------------------------------------

QT       += core gui serialport
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = serialSample
TEMPLATE = app


INCLUDEPATH += source


SOURCES += source/main.cpp\
        source/mainwindow.cpp \
    tcpsocket.cpp

HEADERS  += source/mainwindow.h \
    tcpsocket.h

FORMS    += source/mainwindow.ui
