#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onData();
    void onTimer1();


public:

private:
    Ui::MainWindow *ui;
    QTimer     timer1;

};



#endif // MAINWINDOW_H
